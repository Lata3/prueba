package com.example.calculodesueldo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText IngreseHoras;
    Button Calcular;

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IngreseHoras = (EditText) findViewById(R.id.ETextHorasTrabajadas);
        Calcular =(Button)findViewById(R.id.BtnCalcular);

        Calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Horas = IngreseHoras.getText().toString();
                Intent siguiente = new Intent(MainActivity.this, Sueldo.class);
                siguiente.putExtra("Horastrabajadas", Horas);
                startActivity(siguiente);
            }
        });
    }
}









