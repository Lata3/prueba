package com.example.calculodesueldo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Sueldo extends AppCompatActivity {

    TextView Sueldo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sueldo);
        Sueldo=(TextView)findViewById(R.id.ViewSueldo);

        String valorSueldo= getIntent().getExtras().getString("Horastrabajadas");

        int vs= Integer.parseInt(valorSueldo);
        String result;

         double horas;
         double horas2;

         horas=3;
         horas2=3.25;

        if(vs <=160){
             result = String.valueOf((horas*vs));
             Sueldo.setText(result + " Dolares");

        }else{
            Sueldo.setText(valorSueldo);
            result = String.valueOf((horas2*vs));
            Sueldo.setText(result + " Dolares");
        }
    }
}


